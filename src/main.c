#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<error.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>

#define BUFFER_SIZE 4096
int main(int argc, char* argv[]){
	int src,dest;
	ssize_t bytesRead, bytesWritten;
	ssize_t leidos = 0;
	char *buffer = malloc(sizeof(unsigned char)*BUFFER_SIZE);
	memset(buffer,0,BUFFER_SIZE);
	
	if(argc !=3)		printf("Faltan argumentos|| ./pscopy Ruta_Origen Ruta_Destino\n");	
	else if((src=open(argv[1],O_RDONLY))==-1) {
		perror("Source" );
		return 1;
	}else if((dest=open(argv[2],O_CREAT | O_WRONLY ,0666))==-1){ 
		perror("Dest");
		return 2;
	}else{
		while((bytesRead = read(src,buffer,BUFFER_SIZE))!=0){
			//bytesRead = read(src,buffer,BUFFER_SIZE);

			
			bytesWritten= write(dest,buffer,(ssize_t)bytesRead);
			if(bytesWritten<0) perror("Write");
			bzero(buffer,BUFFER_SIZE);
			leidos+=bytesWritten;
			//printf("%ld \n",leidos);
		}
		printf("\n%ld bytes copiados.\n",leidos);
		close(src);
		close(dest);
		return 0;
	}
	
	
}
